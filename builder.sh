'creation of the ontop bread function'
function painDessus {
  echo '      ___________________________________________________'
  echo '     / / _  _  _  _  _  _  _  _  _  _  _  _  _  _  _  _ \ \'
  echo '    /_/__________________________________________________\_\'
}
'creation of the bottom bread'
function painDessous {
  echo '    ________________________________________________________'
  echo '    \ \ _  _  _  _  _  _  _  _  _  _  _  _  _  _  _  _   / /'
  echo '     \_\________________________________________________/_/'
}

function salad {
    echo '  sssssssssssssssssssssssssss'
}

function tomato {
    echo '  000000000000000000000000000'
}

function ham {
    echo '  @@@@@@@@@@@@@@@@@@@@@@@@@@@'
}


if [ -z "$1" ]
then
  echo "No sandwich specified"
  exit 1
elif [ -z `ls . | grep $1` ]
then
  echo "No sandwich called $1"
  exit 1
fi;

painDessus;
while IFS= read -r ingredient; do
    case `echo $ingredient  | xargs` in
      "jambon")  echo '   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~';;
      "fromage") echo '   === === === === === === === === === === === === === === ==';;
      "butter")  echo '   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ';;
      "ham")echo ham
      "tomato") echo tomato
      "salad") echo salad
      *) echo "Ingredient not found !"
      *)exit 1
    esac
done < "./$1"
painDessous;
